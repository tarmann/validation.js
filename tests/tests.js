module( "validation" );

test( "validators required", function() {
	var v = new validation();

	notEqual( true, v.validators.required({value: ""}), "when passing empty string" );
	ok( v.validators.required({value: "foo"}), "when passing a string" );
	ok( v.validators.required({value: 1}), "when passing a number" );
	ok( v.validators.required({value: 0}), "when passing 0" );
	notEqual( true, v.validators.required({value: NaN}), "when passing NaN" );
});

test( "matches", function() {
	var v = new validation();

	ok( v.validators.matches({value: "foo", matches: "foo"}), "when passing a matched value" );
	notEqual( true, v.validators.matches({value: "foo", matches: ""}), "when passing a empty value" );
});

test("valid_email", function(){
	var v = new validation();

	notEqual( true, v.validators.valid_email({value: ""}), "when passing a empty string" );
	notEqual( true, v.validators.valid_email({value: "foo"}), "when passing a invalid e-mail" );
	ok( v.validators.valid_email({value: "foo@bar.com"}), "when passing a valid e-mail" );
});

test("min_length", function(){
	var v = new validation();

	ok( ! v.validators.min_length({value: "foo", min_length: 5}), "when passing a smaller lenght" );
	ok( v.validators.min_length({value: "this is foo", min_length: 5}), "when passing a larger lenght" );
	ok( v.validators.min_length({value: 123456, min_length:  3}), "when passing number" );	
});

test("max_length", function(){
	var v = new validation();

	ok( v.validators.max_length({value: "foo", max_length: 5}), "when passing a smaller lenght" );
	ok( ! v.validators.max_length({value: "this is foo", max_length: 5}), "when passing a larger lenght" );
	ok( v.validators.max_length({value: 123, max_length: 5 }), "when passing number" );
});

test("exact_length", function(){
	var v = new validation();

	ok( v.validators.exact_length( { value: 'foo', exact_length: 3 } ), "when passing the exact length" );
	ok( ! v.validators.exact_length( { value: 'foo123', exact_length: 3 } ), "when passing the larger length" );
	ok( ! v.validators.exact_length( { value: 'f', exact_length: 3 } ), "when passing the smaller length ");
});

test("greater_than", function(){});
test("less_than", function(){});
test("alpha", function(){});
test("alpha_numeric", function(){});
test("alpha_dash", function(){});
test("numeric", function(){});
test("integer", function(){});
test("decimal", function(){});
test("is_natural", function(){});
test("is_natural_no_zero", function(){});