/*
 * validate.js 0.1
 * Copyright (c) 2013 Bruno Tarmann, http://tarmann.com.br
 * validate.js is open sourced under the MIT license.
 */

(function(window, document, undefined) {

	var validation = function(form, rules, callback){
		this.fields = [];
		this.rules = rules;
		this.customValidators = [];
		this.errors = [];

		// this.validate();
		// callback(this.errors);
	};

	/**
	 * Returns if the object is valid
	 * @returns {Boolean}
	 */

	validation.prototype.validate = function(){

		this.cleanErrors();

		this.validateRules();

		this.validateCustomRules();

		return this.isValid();
	};

	/**
	 * Returns if the object is valid
	 * @returns {Boolean}
	 */

	validation.prototype.validateRules = function(){
		var fields = this.rules;

		for(var i in fields){
			var rules = this.parseRules( fields[i].rules );

			for(var n in rules){
				var rule = rules[n][0],
					params = rules[n].splice(1,1);
					params.unshift(fields[i].value);

				// validator exists?
				if(this.validators[rule] !== undefined){
					var isValid = this.validators[rule](params);	

					if(!isValid){
						console.log(fields[i].name, rule);
						this.addError({ name: fields[i].name, error: rule });
					}
				}
				
				// console.log(fields[i].name, rules[n][0], fields[i].value, rules[n].splice(1,1) );

			}
		}		
	}

	/**
	 * Returns if the object is valid
	 * @returns {Boolean}
	 */

	validation.prototype.validateCustomRules = function(){
		var validators = this.customValidators;

		for(var n in validators){
			var v = validators[n](),
				isValid = (typeof v == 'object') ? v.isValid : v;

			if(!isValid){
				this.addError({ name: n, error: n, response: v });
			}
		}
	};

	/**
	 * Parse rules
	 * @returns {Array} {Object}
	 */

	validation.prototype.parseRules = function(rules){
		var parsedRules = [],
			rulesArray = rules.split('|');

		for(var i in rulesArray){
			var thisRule = rulesArray[i],
				thisRuleArray = [],
				param = thisRule.match(/[^[\]]+(?=])/g);

			if(param){
				thisRuleArray.push(thisRule.substring(0, thisRule.indexOf('[')))
				thisRuleArray.push(param);
			} else {
				thisRuleArray.push(thisRule);
			}

			parsedRules.push( thisRuleArray );
		}

		return parsedRules;
	};

	/**
	 * Return list of errors
	 * @returns {Object}
	 */
	validation.prototype.getErrors = function(){
		return this.errors;
	};

	/**
	 * Returns if the object is valid
	 * @returns {Boolean}
	 */
	validation.prototype.isValid = function(){
		return (this.errors.length > 0);
	};

	/**
	 * addError
	 * @param {Object} error
	 * @returns {Array} errors
	 */
	validation.prototype.addError = function(error){
		return this.errors.push(error);
	};

	/**
	 * cleanErrors
	 * @returns {Array} errors
	 */
	validation.prototype.cleanErrors = function(){
		this.errors = [];
		return this.errors;
	};

	/**
	 * mapFields
	 *
	 */
	validation.prototype.mapAllFields = function(){
		this.fields = [];

		// mappers
		for(var i in this.fieldMappers){
			// selectors
			for(var j in this.fieldMappers[i].map){
				var field = this.fieldMappers[i].map[j];
				this.mapField( this.fieldMappers[i].mapper( j, field ) );
			}
		}

		return this.fields.length;
	};

	/**
	 * mapField
	 * @param {Object} field
	 *
	 */
	validation.prototype.mapField = function(field){
		this.fields.push(field);
	};

	/**
	 * cleanErrors
	 * @param {String} name
	 * @param {Function} callback
	 * @returns {Number} total of custom validators
	 */
	validation.prototype.registerCallback = function(name, callback){
		this.customValidators[name] = callback;
		return this.customValidators.length;
	};

	/**
	 * registerFieldMapper
	 * @param {String} name
	 * @param {Function} validator_name
	 * @param {Function} callback
	 */
	validation.prototype.registerFieldMapper = function(name, validator_name, callback){
	};

	/**
	 * fieldMappers
	 * 
	 */
	validation.prototype.fieldMappers = [{
		map: function(){
			var map = [],
				defaultValidators = this.validators;

			for(var name in defaultValidators){
				map[name] = name;
			}

			return map;
		},
		mapper: function(selector, rule){
			return $('[data-val-'+ selector +']').map(function(el, index){
				return {
					name : $(this).attr('name'),
					rule : rule,
					value : $(this).val()
				};
			});
		}
	}];

	/**
	 * validators
	 * 
	 */
	validation.prototype.validators = {
		required : function(params){
			return (params[0] !== "");
		},
		matches : function(params){
			return (params[0] == params[1]);
		},
		valid_email : function(params){
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(params[0]);
		},
		min_length : function(params){
			var value = params[0].toString();
			return (value.length >= params[1]);
		},
		max_length : function(params){
			var value = params[0].toString();
			return (value.length <= params[1]);
		},
		exact_length : function(params){
			return (params[0].length == params[1]);
		},
		greater_than : function(params){
			return (params[0] > params[1]);
		},
		less_than : function(params){
			return (params[0] < params[1]);
		},
		alpha : function(params){

		},
		alpha_numeric : function(params){

		},
		alpha_dash : function(params){

		},
		numeric : function(params){

		},
		integer : function(params){

		},
		decimal : function(params){

		},
		is_natural : function(params){

		},
		is_natural_no_zero : function(params){

		}
	};

    window.validation = validation;

})(window, document);